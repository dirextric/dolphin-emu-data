%global dolphin_revision REPLACEME_DOLPHIN_REVISION
%global dolphin_revision_description REPLACEME_DOLPHIN_DESCRIPTION

%if 0%{?fedora}
%undefine _cmake_shared_libs
%endif

%if 0%{?suse_version}
%define __builder ninja
%endif

Name:             dolphin-emu-git
Summary:          Dolphin, a GameCube and Wii Emulator
Version:          REPLACEME_VERSION
Release:          1%{?dist}
License:          GPLv2+
URL:              https://dolphin-emu.org/
Source0:          https://github.com/dolphin-emu/dolphin/archive/%{dolphin_revision}.tar.gz

BuildRequires:    pkgconfig(alsa)
BuildRequires:    pkgconfig(bluez)

%if 0%{?fedora}
BuildRequires:    bochs-devel
%endif

BuildRequires:    pkgconfig(bzip2)
BuildRequires:    cmake

%if 0%{?fedora}
BuildRequires:    cubeb-devel
%endif

%if 0%{?use_ffmpeg}
BuildRequires:    pkgconfig(libavcodec)
BuildRequires:    pkgconfig(libavformat)
BuildRequires:    pkgconfig(libavutil)
BuildRequires:    pkgconfig(libswscale)
%endif

BuildRequires:    pkgconfig(fmt)

%if 0%{?fedora} || 0%{!?sle_version:1}
BuildRequires:    gcc-c++ >= 9
%endif
%if 0%{?sle_version}
BuildRequires:    gcc9-c++
%endif

BuildRequires:    gettext
BuildRequires:    pkgconfig(gl)
BuildRequires:    glslang-devel
BuildRequires:    pkgconfig(hidapi-hidraw)
BuildRequires:    pkgconfig(libcurl)
BuildRequires:    pkgconfig(libenet)
BuildRequires:    pkgconfig(libevdev)
BuildRequires:    pkgconfig(liblzma)
BuildRequires:    pkgconfig(libpulse)
BuildRequires:    pkgconfig(libsystemd)
BuildRequires:    pkgconfig(libudev)
BuildRequires:    pkgconfig(libusb-1.0)
BuildRequires:    pkgconfig(libxxhash)
BuildRequires:    pkgconfig(libzstd)
BuildRequires:    pkgconfig(lzo2)
BuildRequires:    mbedtls-devel

%if 0%{?fedora}
BuildRequires:    pkgconfig(miniupnpc)
%endif
%if 0%{?suse_version}
BuildRequires:    libminiupnpc-devel
%endif

BuildRequires:    pkgconfig(minizip)

%if 0%{?fedora}
BuildRequires:    ninja-build
%endif
%if 0%{?suse_version}
BuildRequires:    ninja
%endif

BuildRequires:    picojson-devel
BuildRequires:    pkgconfig(pugixml)

%if 0%{?fedora}
BuildRequires:    qt5-qtbase-devel
BuildRequires:    qt5-qtbase-private-devel
%endif
%if 0%{?suse_version}
BuildRequires:    libqt5-qtbase-devel
BuildRequires:    libqt5-qtbase-private-headers-devel
%endif

BuildRequires:    pkgconfig(sdl2)
BuildRequires:    pkgconfig(sfml-all)
BuildRequires:    pkgconfig(soundtouch)

%if 0%{?suse_version}
BuildRequires:    pkgconfig(spirv-cross-c-shared)
%endif

%if 0%{?fedora}
BuildRequires:    spirv-headers-devel
%endif
%if 0%{?suse_version}
BuildRequires:    spirv-headers
%endif

BuildRequires:    pkgconfig(SPIRV-Tools)
BuildRequires:    spirv-tools

%if 0%{?fedora}
BuildRequires:    pkgconfig(spng)
%endif

BuildRequires:    systemd-rpm-macros

BuildRequires:    pkgconfig(vulkan)
BuildRequires:    pkgconfig(xi)
BuildRequires:    pkgconfig(xrandr)

%if 0%{?fedora} || 0%{!?sle_version:1}
BuildRequires:    pkgconfig(zlib-ng)
%endif

BuildRequires:    hicolor-icon-theme

ExclusiveArch:    x86_64 aarch64

%description
Dolphin is an emulator for two Nintendo video game consoles, GameCube and the Wii.
It allows PC gamers to enjoy games for these two consoles in full HD with several
enhancements such as compatibility with all PC controllers, turbo speed,
networked multiplayer, and more.
Most games run perfectly or with minor bugs.

%if 0%{?suse_version}
%debug_package
%endif

%prep
%autosetup -n dolphin-%{dolphin_revision}

%build
%if 0%{?sle_version}
export CC=/usr/bin/gcc-9
export CXX=/usr/bin/g++-9
%endif
%cmake \
%if 0%{?suse_version}
    -DBUILD_SHARED_LIBS:BOOL=OFF \
    -DBUILD_STATIC_LIBS:BOOL=ON \
%endif
    -DCMAKE_C_FLAGS="-isystem /usr/include/minizip ${CFLAGS}" \
    -DCMAKE_CXX_FLAGS="-isystem /usr/include/minizip ${CXXFLAGS}" \
    -DCMAKE_SIZEOF_VOID_P=8 \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DDOLPHIN_WC_BRANCH=master \
    -DDOLPHIN_WC_DESCRIBE=%{dolphin_revision_description} \
    -DDOLPHIN_WC_REVISION=%{dolphin_revision} \
    -DUSE_SHARED_ENET=ON \
    -DXXHASH_FOUND=ON \
    -GNinja
%cmake_build

%install
%cmake_install
mkdir -p %{buildroot}%{_udevrulesdir}
install -Dm 644 Data/51-usb-device.rules %{buildroot}%{_udevrulesdir}/51-dolphin-usb-device.rules
%find_lang dolphin-emu

%files -f dolphin-emu.lang
%doc Readme.md
%license COPYING
%{_bindir}/dolphin-emu
%{_bindir}/dolphin-emu-nogui
%{_bindir}/dolphin-tool
%{_datadir}/dolphin-emu/
%{_datadir}/applications/dolphin-emu.desktop
%{_datadir}/icons/hicolor/*/apps/dolphin-emu.*
%{_mandir}/man6/dolphin-emu.*
%{_mandir}/man6/dolphin-emu-nogui.*
%{_udevrulesdir}/51-dolphin-usb-device.rules

%changelog
